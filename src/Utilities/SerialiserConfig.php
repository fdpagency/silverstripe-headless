<?php

namespace FDP\Headless\Utilities;

use SilverStripe\Core\Config\Config;
use SilverStripe\View\ArrayData;
use SilverStripe\View\ViewableData;

class SerialiserConfig extends ViewableData
{

    private static $named_configs = [];

    public static function create(...$args)
    {
        $config = new SerialiserConfig();
        if (count($args) > 0) {
            $config = $config->registerByName($args[0], $config);
        }
        return $config;
    }

    private $types;

    public function __construct()
    {
        parent::__construct();
        $this->types = [];
    }

    public function registerType($type, $includes = null, $excludes = null, $config = null)
    {
        $this->types[$type] = ArrayData::create([
            'Includes' => $includes,
            'Excludes' => $excludes,
            'ChildConfig' => $config
        ]);
        return $this;
    }

    public function getIncludes($type)
    {
        if (array_key_exists($type, $this->types)) {
            return $this->types[$type]->Includes;
        } else {
            foreach ($this->types as $t => $o) {
                if (is_subclass_of($type, $t)) {
                    return $this->types[$t]->Includes;
                }
            }
            return null;
        }
    }

    public function getExcludes($type)
    {
        if (array_key_exists($type, $this->types)) {
            return $this->types[$type]->Excludes;
        } else {
            foreach ($this->types as $t => $o) {
                if (is_subclass_of($type, $t)) {
                    return $this->types[$t]->Excludes;
                }
            }
            return null;
        }
    }

    public function getChildConfig($type)
    {
        if (array_key_exists($type, $this->types)) {
            return $this->types[$type]->ChildConfig;
        } else {
            foreach ($this->types as $t => $o) {
                if (is_subclass_of($type, $t)) {
                    return $this->types[$t]->ChildConfig;
                }
            }
            return null;
        }
    }

    public function registerByName($name)
    {
        return $this->buildByName($name, $this);
    }

    private function buildByName($name, $config = null)
    {
        return $this->buildFromData(
            Config::inst()->get(get_class($this), 'named_configs')[$name],
            $config
        );
    }

    private function buildFromData($data, $config = null)
    {
        if (is_null($config)) {
            $config = SerialiserConfig::create();
        }
        foreach ($data as $type) {
            if (array_key_exists('name', $type)) {
                $config = $this->buildByName($type['name'], $config);
            } elseif (array_key_exists('type', $type)) {
                $config = $config->registerType(
                    $type['type'],
                    array_key_exists('includes', $type) ? $type['includes'] : null,
                    array_key_exists('excludes', $type) ? $type['excludes'] : null,
                    array_key_exists('config', $type) ? $this->buildFromData($type['config']) : null
                );
            }
        }
        return $config;
    }
}
