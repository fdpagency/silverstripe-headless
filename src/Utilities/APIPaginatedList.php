<?php

namespace FDP\Headless\Utilities;

use SilverStripe\ORM\PaginatedList;

class APIPaginatedList extends PaginatedList
{
    public function getPageStart()
    {
        if ($this->pageStart === null) {
            if ($page = $this->getRequest()->getVar('Page')) {
                $this->pageStart = ($page - 1) * $this->getPageLength();
            } else {
                $this->pageStart = 0;
            }
        }
        return $this->pageStart;
    }

    public function serialise($config = null)
    {
        $length = $this->getPageLength();
        $records = array();
        if ($this->limitItems && $length) {
            $list = clone $this->list;
            $records = $list->limit($length, $this->getPageStart())->serialise($config);
        } else {
            $records = $this->list->serialise($config);
        }
        $current = $this->CurrentPage();
        $total = $this->TotalPages();
        $pages = array();
        $last = null;
        for ($i = 1; $i <= $total; $i++) {
            if ($i <= 5 || ($i >= $current - 2 && $i <= $current + 2) || $i >= $total - 5) {
                if ($i != $last + 1) {
                    $pages[] = array(
                        'Number' => '...',
                        'IsSpacer' => true,
                        'IsCurrentPage' => false
                    );
                }
                $pages[] = array(
                    'Number' => $i,
                    'IsSpacer' => false,
                    'IsCurrentPage' => $i == $current
                );
                $last = $i;
            }
        }
        return array(
            'Records' => $records,
            'CurrentPage' => $current,
            'TotalPages' => $total,
            'FirstRecord' => $this->FirstItem(),
            'LastRecord' => $this->LastItem(),
            'TotalRecords' => $this->getTotalItems(),
            'IsFirstPage' => $current == 1,
            'IsLastPage' => $current == $this->TotalPages(),
            'NextPage' => $this->NotLastPage() ? $current + 1 : null,
            'PreviousPage' => $this->NotFirstPage() ? $current - 1 : null,
            'Pages' => $pages
        );
    }
}
