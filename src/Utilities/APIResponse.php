<?php

namespace FDP\Headless\Utilities;

use FDP\Headless\ViewModels\APIValidationResult;

use Monolog\Handler\HandlerInterface;

use SilverStripe\Control\Director;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Convert;
use SilverStripe\Core\Injector\Injector;

class APIResponse extends HTTPResponse
{

    private static $origins = array();

    private static $instance;

    public static function inst()
    {
        if (is_null(self::$instance)) {
            self::$instance = new APIResponse();
        }
        return self::$instance;
    }

    private $payload = array();

    public function __construct($payload = array(), $status = null, $description = null)
    {
        parent::__construct('', $status, $description);
        $this->addHeader('Content-Type', 'text/javascript');
        $this->payload = $payload;
        $origins = Config::inst()->get(APIResponse::class, 'origins');
        if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] != '') {
            foreach ($origins as $origin) {
                if (preg_match('#' . $origin . '#', $_SERVER['HTTP_ORIGIN'])) {
                    $this->addHeader('Access-Control-Allow-Origin', $_SERVER['HTTP_ORIGIN']);
                    $this->addHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
                    $this->addHeader('Access-Control-Max-Age', '1000');
                    $this->addHeader(
                        'Access-Control-Allow-Headers',
                        'Content-Type, Authorization, X-Requested-With, User-Identifier, User-Token'
                    );
                    break;
                }
            }
        }
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function modifyPayload(callable $callback)
    {
        $callback($this->payload);
        return $this;
    }

    public function setResult(APIValidationResult $result)
    {
        if (!array_key_exists('Result', $this->payload)) {
            $this->payload['Result'] = $result;
        } else {
            $this->payload['Result']->merge($result);
        }
        return $this;
    }

    public function setStatusCode($code, $description = null)
    {
        parent::setStatusCode($code, $description);
        if ($code >= 400 && $code <= 499) {
            $this->modifyPayload(function (&$payload) {
                $payload = array_merge(
                    array('Result' => APIValidationResult::create()),
                    $payload
                );
            });
        }
    }

    protected function outputBody()
    {
        if ($this->isError() && $this->statusCode == 500) {
            $body = $this->getBody();
            if (empty($body)) {
                $handler = Injector::inst()->get(HandlerInterface::class);
                $formatter = $handler->getFormatter();
                echo $formatter->format(array(
                    'code' => $this->statusCode
                ));
            } else {
                echo $body;
            }
        } else {
            $payload = $this->payload;
            if (array_key_exists('Result', $payload)) {
                $payload['Result'] = $payload['Result']->serialise(SerialiserConfig::create('validation-result'));
            }
            echo Convert::raw2json($payload);
        }
    }
}
