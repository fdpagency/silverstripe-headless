<?php

namespace FDP\Headless\Utilities;

use FDP\Common\ViewModels\ViewModel;
use FDP\Headless\Extensions\SerialisableObject;

use SilverStripe\Core\Config\Config;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataList;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBBoolean;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\FieldType\DBFloat;
use SilverStripe\ORM\FieldType\DBHTMLText;
use SilverStripe\ORM\RelationList;

class Serialiser
{

    public static function serialise_object($object, $config = null)
    {
        $obj = array();
        $includes = !is_null($config) ? $config->getIncludes(get_class($object)) : null;
        $excludes = !is_null($config) ? $config->getExcludes(get_class($object)) : null;
        foreach (Config::inst()->get(get_class($object), 'serialised_fields') as $property) {
            $include = true;
            if (is_array($includes) && !in_array($property, $includes)) {
                $include = false;
            }
            if (is_array($excludes) && in_array($property, $excludes)) {
                $include = false;
            }
            if ($include) {
                $db_obj = $object->hasMethod('dbObject') ? $object->dbObject($property) : null;
                if (is_a($db_obj, DBBoolean::class)) {
                    $obj[$property] = (boolean)$db_obj->getValue();
                } elseif (is_a($db_obj, DBFloat::class)) {
                    $obj[$property] = (float)$db_obj->getValue();
                } elseif (is_a($db_obj, DBHTMLText::class)) {
                    $obj[$property] = $db_obj->forTemplate();
                } else {
                    $value = null;
                    $method = sprintf('get%s', $property);
                    if ($object->hasMethod($method)) {
                        $value = $object->$method();
                    } elseif ($object->hasMethod($property)) {
                        $value = $object->$property();
                    }
                    if (is_null($value)) {
                        $obj[$property] = $object->$property;
                    } else {
                        $obj[$property] = self::serialise_value($value, $object, $config);
                    }
                }
            }
        }
        return $obj;
    }

    private static function serialise_value($value, $object, $config = null)
    {
        if (is_a($value, RelationList::class) || is_a($value, DataList::class) || is_a($value, ArrayList::class)) {
            return $value->serialise(!is_null($config) ? $config->getChildConfig(get_class($object)) : null);
        } elseif (is_a($value, DataObject::class)) {
            if ($value->ID < 1) {
                return null;
            }
            return $value->serialise(!is_null($config) ? $config->getChildConfig(get_class($object)) : null);
        } elseif (is_a($value, DBField::class) || is_a($value, ViewModel::class)) {
            return $value->serialise(!is_null($config) ? $config->getChildConfig(get_class($object)) : null);
        } elseif (is_array($value)) {
            $list = array();
            foreach ($value as $k => $v) {
                $list[$k] = self::serialise_value($v, $object, $config);
            }
            return $list;
        } elseif (!is_object($value)) {
            return $value;
        }
    }

    public static function unserialise_data($data, $serialisable)
    {
        $unserialised = array();
        foreach (Config::inst()->get($serialisable, 'serialised_fields') as $property) {
            if (array_key_exists($property, $data)) {
                $value = $data[$property];
                if (is_array($value)) {
                    $config = Config::inst();
                    $child_serialisable = null;
                    $has_one = $config->get($serialisable, 'has_one');
                    $list = true;
                    $has_many = $config->get($serialisable, 'has_many');
                    $many_many = $config->get($serialisable, 'many_many');
                    $belongs_many_many = $config->get($serialisable, 'belongs_many_many');
                    if (is_array($has_one) && array_key_exists($property, $has_one)) {
                        $child_serialisable = $has_one[$property];
                        $list = false;
                    } elseif (is_array($has_many) && array_key_exists($property, $has_many)) {
                        $child_serialisable = $has_many[$property];
                    } elseif (is_array($many_many) && array_key_exists($property, $many_many)) {
                        $child_serialisable = $many_many[$property];
                    } elseif (is_array($belongs_many_many) && array_key_exists($property, $belongs_many_many)) {
                        $child_serialisable = $belongs_many_many[$property];
                    }
                    if (!is_null($child_serialisable)) {
                        if ($list) {
                            $unserialised[$property] = array();
                            foreach ($value as $child) {
                                $unserialised[$property][] = self::unserialise_data($child, $child_serialisable);
                            }
                        } else {
                            $unserialised[$property] = self::unserialise_data($value, $child_serialisable);
                        }
                    }
                } else {
                    $unserialised[$property] = $value;
                }
                unset($data[$property]);
            }
        }
        return array_merge($unserialised, $data);
    }

    public static function serialise_list($list, $config)
    {
        $serialised = array();
        foreach ($list as $obj) {
            if ($obj->hasExtension(SerialisableObject::class)) {
                $serialised[] = $obj->serialise($config);
            }
        }
        return $serialised;
    }
}
