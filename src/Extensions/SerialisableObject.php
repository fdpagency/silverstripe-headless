<?php

namespace FDP\Headless\Extensions;

use FDP\Headless\Utilities\Serialiser;

use SilverStripe\ORM\DataExtension;

class SerialisableObject extends DataExtension
{
    private static $serialised_fields = array();

    public function serialise($config = null)
    {
        if (is_null($config) && $this->owner->hasMethod('getSerialiserConfig')) {
            $config = $this->owner->getSerialiserConfig();
        }
        $this->owner->extend('updateSerialiserConfig', $config);
        return Serialiser::serialise_object($this->owner, $config);
    }
}
