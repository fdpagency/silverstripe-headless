<?php

namespace FDP\Headless\Extensions;

use FDP\Headless\Utilities\Serialiser;

use SilverStripe\Core\Extension;

class SerialisableList extends Extension
{
    public function serialise($config = null)
    {
        if (is_null($config) && $this->owner->hasMethod('getSerialiserConfig')) {
            $config = $this->owner->getSerialiserConfig();
            $this->owner->extend('updateSerialiserConfig', $config);
        }
        return Serialiser::serialise_list($this->owner, $config);
    }
}
