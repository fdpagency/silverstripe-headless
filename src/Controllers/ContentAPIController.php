<?php

namespace FDP\Headless\Controllers;

use FDP\Headless\Traits\APIController;
use FDP\Headless\Utilities\APIResponse;
use FDP\Headless\Utilities\SerialiserConfig;

use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\CMS\Controllers\ModelAsController;
use SilverStripe\CMS\Controllers\RootURLController;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Security\Member;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Versioned\Versioned;

class ContentAPIController extends Controller
{
    use APIController;

    public function Index(HTTPRequest $request)
    {
        if ($this->shouldInclude('site')) {
            $menu_pages = SiteTree::get()->filter([
                'ParentID' => 0,
                'ShowInMenus' => true
            ]);
            $menu = ['Items' => []];
            foreach ($menu_pages as $page) {
                $menu['Items'][] = $page->serialise(SerialiserConfig::create('menu-page'));
            }
            $this->getResponse()->modifyPayload(function (&$payload) use ($menu) {
                $payload['Site'] = [
                    'Config' => SiteConfig::current_site_config()->serialise(),
                    'Menu' => $menu
                ];
            });
        }
        if ($url = $request->getVar('url')) {
            Versioned::choose_site_stage($request);
            $rules = [
                '' => RootURLController::class,
                '$URLSegment//$Action/$ID/$OtherID' => ModelAsController::class
            ];
            $req = new HTTPRequest(
                $request->httpMethod(),
                $url,
                $request->getVars(),
                $request->postVars(),
                $request->getBody()
            );
            $req->setSession($request->getSession());
            foreach ($rules as $pattern => $type) {
                $args = $req->match($pattern, true);
                if ($args == false) {
                    continue;
                }
                $req->setRouteParams(['Controller' => $type]);

                $controller = Injector::inst()->create($type);
                $response = $controller->handleRequest($req);
                if (get_class($response) == APIResponse::class || (
                    is_a($response, HTTPResponse::class)
                    &&
                    Director::isDev()
                    &&
                    $request->getVar('debug') == 1
                )) {
                    return $response;
                }
            }
            return $this->getResponse()->modifyPayload(function (&$payload) use ($url) {
                $payload['GlobalMessages'] = ["Page not found: '{$url}'"];
            })->setStatusCode(404);
        } else {
            return $this->getResponse()->modifyPayload(function (&$payload) {
                $payload['GlobalMessages'] = ['No URL provided'];
            })->setStatusCode(400);
        }
    }
}
