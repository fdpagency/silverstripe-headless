<?php

namespace FDP\Headless\Traits;

use FDP\Headless\Utilities\APIResponse;

use SilverStripe\Control\HTTPRequest;

trait APIController
{
    protected function beforeHandleRequest(HTTPRequest $request)
    {
        $this->setRequest($request);
        $this->pushCurrent();
        $this->setResponse(APIResponse::inst());
        $this->doInit();
    }

    public function handleRequest(HTTPRequest $request)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            return new APIResponse();
        } else {
            return parent::handleRequest($request);
        }
    }

    public function shouldInclude($component)
    {
        if ($excludes = $this->getRequest()->getVar('excludes')) {
            $excludes = explode(',', $excludes);
            return !in_array($component, $excludes);
        }
        return true;
    }

    public function unauthenticated()
    {
        return $this->getResponse()->modifyPayload(function (&$payload) {
            $payload['GlobalMessages'] = ['Unauthorised user'];
        })->setStatusCode(401);
    }

    public function unauthorised()
    {
        $this->getResponse()->modifyPayload(function (&$payload) {
            $payload['GlobalMessages'] = ['Permission denied'];
        })->setStatusCode(403);
    }
}
