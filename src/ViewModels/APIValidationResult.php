<?php

namespace FDP\Headless\ViewModels;

use FDP\Common\ViewModels\ViewModel;

use SilverStripe\ORM\ValidationResult;

class APIValidationResult extends ViewModel
{
    private $messages = [];

    public function getMessages()
    {
        return $this->messages;
    }

    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    public function addMessage($message)
    {
        $this->messages[] = $message;
    }

    private $fields = [];

    public function getFields()
    {
        return $this->fields;
    }

    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    public function addField($name, $result)
    {
        $this->fields[$name] = $result;
    }

    public function merge($result)
    {
        if (is_a($result, ValidationResult::class)) {
            foreach ($result->getMessages() as $field) {
                $this->addField($field['fieldName'], new APIValidationResult(['Messages' => [$field['message']]]));
            }
        } else {
            foreach ($result->Messages as $message) {
                $this->addMessage($message);
            }
            foreach ($result->Fields as $name => $result) {
                $this->addField($name, $result);
            }
        }

        return $this;
    }
}
